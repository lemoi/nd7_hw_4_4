angular
    .module('PokemonApp')
    .factory('PokemonsService', function($resource, $http) {

        // Не получилось переопределить заголовки для getBerries
        var headersBackendless = {
            "application-id": "4B730C92-F81E-236B-FFF0-6651FE882800",
            "secret-key": "CB6DE86C-6069-86C4-FF1C-9049D5AC9400"
        };
        //$http.defaults.headers.common = headersBackendless;

        return $resource(
            'https://api.backendless.com/v1/data/pokemon/:pokemonId/',
            {
                pokemonId: '@pokemonId'
            },
            {
                query: {
                    method: 'GET',
                    isArray: true,
                    headers: headersBackendless,
                    transformResponse: function(responseData) {
                        return angular.fromJson(responseData).data;
                    }
                },
                update: {
                    headers: headersBackendless,
                    method: 'PUT'
                },
                save: {
                    headers: headersBackendless,
                    method: 'POST'
                },
                get: {
                    headers: headersBackendless,
                    method: 'GET'
                },
                getBerries: {
                    method: 'GET',
                    isArray: true,
                    headers: {},
                    url: 'http://pokeapi.co/api/v2/berry/:berryId/',
                    transformResponse: function(responseData) {
                        return angular.fromJson(responseData).results;
                    }
                }
            })
    });
