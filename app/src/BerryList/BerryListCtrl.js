'use strict';

pokemonApp.controller('BerryListCtrl', function($scope, PokemonsService) {

    $scope.berries = PokemonsService.getBerries({limit: 7});

});
